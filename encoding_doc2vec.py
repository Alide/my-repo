#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Sep 24 23:18:26 2020

@author: alidexiang
"""
import os
import numpy as np
from gensim.models.doc2vec import Doc2Vec
from utils.get_chunks import get_chunks_array
from ir.train_d2v import token_clean


def encode_chunks(vect_size, folder='chunks', labels_dest='labels.npy'):
    """ Encodes chunks with doc2vec

    Args:
        vect_size (int) : the vector's size.
        folder (str, optional): the chunks folder.
        labels_dest (str): the in which we save labels
    """
    features_dest = 'encoded_' + str(vect_size) + '_trained_doc2vec.npy'
    model = Doc2Vec.load("d2v" + str(vect_size) + ".model")
    _, labels = get_chunks_array(folder)
    list_encoded_chunks = []
    for label in labels:
        list_encoded_chunks.append(model.docvecs[label].tolist())
    cwd = os.getcwd()
    np.save(os.path.join(cwd, features_dest), np.array(list_encoded_chunks))
    np.save(os.path.join(cwd, labels_dest), labels)


def encode_question(question, model):
    Tok_question = token_clean(question)
    Encoded_question = model.infer_vector(Tok_question)
    return Encoded_question
